import React, { useState } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Login from "./screens/Login";
import Main from "./screens/Main";

type RootStackParamList = {
  Login: undefined;
  Main: undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

const App = () => {
  const [isLoggedIn, setLoggedIn] = useState(false);

  const handleLogin = () => {
    setLoggedIn(true);
  };

  const handleMain = (navigation: any) => {
    navigation.navigate('Main');
  };
  
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={isLoggedIn ? "Main" : "Login"}>
        <Stack.Screen
            name="Login"
            component={(props) => (
              <Login {...props} onLogin={handleLogin} />
            )}
        />
        <Stack.Screen
            name="Main"
            component={(props) => (
              <Main {...props} onMain={handleMain} />
            )}
            options={{ headerShown: false }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

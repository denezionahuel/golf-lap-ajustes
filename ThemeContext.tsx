// import React, { createContext, useState } from "react";

// type ThemeContextType = {
//   isDarkMode: boolean;
//   toggleDarkMode: () => void;
// };

// export const ThemeContext = createContext<ThemeContextType>({
//   isDarkMode: false,
//   toggleDarkMode: () => {},
// });

// export const ThemeProvider: React.FC = ({ children }) => {
//   const [isDarkMode, setIsDarkMode] = useState(false);

//   const toggleDarkMode = () => {
//     setIsDarkMode(!isDarkMode);
//   };

//   return (
//     <ThemeContext.Provider
//       value={{
//         isDarkMode,
//         toggleDarkMode,
//       }}
//     >
//       {children}
//     </ThemeContext.Provider>
//   );
// };
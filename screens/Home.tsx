import React from 'react';
import { FlatList, StyleSheet, Text, ScrollView, View } from 'react-native';

type Props = {
  onHome: () => void;
};

const Home: React.FC<Props> = ({ onHome }) => {
  const items = [
    {
      id: '1',
      title: 'WALA 2023: 60 golfistas amateurs con la ilusión de jugar tres majors',
      description: 'El Women´s Amateur Latin America (WALA) celebrará su tercera edición del 15 al 18 de noviembre de 2023. El campeonato, presentado por The R&A y la ANNIKA Foundation, se realizará en Pilar Golf, Buenos Aires, y contará con las 60 mejores golfistas amateurs del WAGR® de Latinoamérica y el Caribe. En esta tercera edición, habrá 14 países representados: Argentina, Barbados, Bolivia, Brasil, Chile, Colombia, Ecuador, Islas Caimán, México, Panamá, Paraguay, Perú, Uruguay y Venezuela.',
    },
    {
      id: '2',
      title: 'Cómo ver la Ryder Cup de golf en STAR+',
      description: 'La edición N°44 de la tradicional Ryder Cup, con su fisonomía tan singular de enfrentar bajo la modalidad de match play a los poderosos equipos de Europa y de Estados Unidos, tendrá desde el viernes 29 de septiembre hasta el domingo 1° de octubre por escenario el Marco Simone Golf & Country Club de Roma, Italia.',
    },
    {
      id: '3',
      title: 'Gran birdie de Emiliano Grillo para seguir soñando',
      description: 'El argentino bajó el par del hoyo 9 en el BMW Championship y continuaba con su pelea para terminar entre los 30 que se meterán en el torneo final del playoff de PGA.',
    },
  ];

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.home}>Home</Text>
      {items.map((item) => {
        return (
          <View style={styles.itemContainer} key={item.id}>
            <View style={styles.item}>
              <Text style={styles.title}>{item.title}</Text>
              <Text style={styles.description}>{item.description}</Text>
            </View>
          </View>
        );
      })}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    paddingTop: 0,
    paddingHorizontal: 10,
    justifyContent: 'center',
  },
  itemContainer: {
    marginBottom: 10,
  },
  item: {
    backgroundColor: '#E32D39',
    borderRadius: 10,
    padding: 10,
  },
  title: {
    fontSize: 18,
    color: 'black',
    textAlignVertical: 'center',
    marginBottom: 10,
  },
  description: {
    fontSize: 12,
    color: 'black',
    textAlignVertical: 'center',
    marginBottom: 7,
  },
  home: {
    fontSize: 20,
    color: 'black',
    padding: 4,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    marginTop: 10,
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

export default Home;
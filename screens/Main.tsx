import * as React from 'react';
import { Button, Text, View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import SelectPlayers from "./SelectPlayers";
import Scorecard from "./Scorecard";
import Settings from "./Settings";
import Home from "./Home";
import headerIcon from '../assets/icon.png';


const GameStack = createStackNavigator();

function GameStackScreen() {
  return (
    <GameStack.Navigator>
      <GameStack.Screen name="SelectPLayers" component={SelectPlayers} />
      <GameStack.Screen name="Scorecard" component={Scorecard} />
    </GameStack.Navigator>
  );
}
const Tab = createBottomTabNavigator();

function HeaderLogo() {
  return (
    <View style={{flex: 1, flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'center',}}>
      <Image
        style={{ width: 50, height: 50 }}
        source={headerIcon}
      />
      <Text style={{color: 'black', padding: 5, fontSize: 18}}>La Pampa Golf</Text>
    </View>
  );
}

export default function Main() {
  return (
      <Tab.Navigator 
        screenOptions={{ headerShown: false }}
        tabBarOptions={{ showLabel: false }}
      >
        <Tab.Screen 
          name="Home" 
          component={Home} 
          options={{
            tabBarIcon: ({ color, size }) => (
              <AntDesign name='home' color={color} size={size} />
            )
          }}          
        />
        <Tab.Screen 
          name="Game" 
          component={GameStackScreen} 
          options={{
            tabBarIcon: ({ color, size }) => (
              <Ionicons name='golf' size={size} color={color} />
            )
          }}          
        />
        <Tab.Screen 
          name="Settings" 
          component={Settings} 
          options={{
            tabBarIcon: ({ color, size }) => (
              <AntDesign name='setting' size={size} color={color} />
            )
          }}          
        />
      </Tab.Navigator>
  );
}
import { StackActions, useNavigation } from '@react-navigation/native';
import React, { useContext, useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

type Props = {
  onSettings: () => void;
};

const Settings: React.FC<Props> = ({ onSettings }) => {
  const navigation = useNavigation();
  const [isDarkMode, setIsDarkMode] = useState(false);

  const handleLogout = () => {
    navigation.dispatch(StackActions.popToTop());
  };

  //modo oscuro que todavia esta a prueba
  const toggleDarkMode = () => {
    setIsDarkMode(!isDarkMode);
  };

  return (
    <View style={[styles.container, isDarkMode ? styles.darkContainer : styles.lightContainer]}>
      <Text style={[styles.text, isDarkMode ? styles.darkText : styles.lightText]}>Ajustes</Text>
      <TouchableOpacity style={[styles.button, isDarkMode ? styles.darkButton : styles.lightButton]} onPress={handleLogout}>
        <Text style={[styles.buttonText, isDarkMode ? styles.darkButtonText : styles.lightButtonText]}>Salir</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.button, isDarkMode ? styles.darkButton : styles.lightButton]} onPress={toggleDarkMode}>
        <Text style={[styles.buttonText, isDarkMode ? styles.darkButtonText : styles.lightButtonText]}>
          {isDarkMode ? 'Modo Claro' : 'Modo Oscuro'}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    paddingTop: 30,
    backgroundColor: '#ffffff',
  },
  darkContainer: {
    backgroundColor: '#000000',
  },
  lightContainer: {
    backgroundColor: '#ffffff',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#000000',
  },
  darkText: {
    color: '#ffffff',
  },
  lightText: {
    color: '#000000',
  },
  button: {
    backgroundColor: '#E32D39',
    borderRadius: 4,
    padding: 10,
    width: 130,
    alignSelf: 'center',
    marginTop: 20,
  },
  darkButton: {
    backgroundColor: '#1f1f1f',
  },
  lightButton: {
    backgroundColor: '#E32D39',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
  },
  darkButtonText: {
    color: '#ffffff',
  },
  lightButtonText: {
    color: 'white',
  },
});

export default Settings;
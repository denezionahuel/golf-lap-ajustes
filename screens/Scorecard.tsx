import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, Text, TextInput } from 'react-native';
import { Table, Row } from 'react-native-table-component';

export default class App extends Component {
  constructor(props: {} | Readonly<{}>) {
    super(props);
    this.state = {
      tableHead: ['Hoyos', 'Par', 'HCP', 'Jugador', 'Marcador'],
      widthArr: [68, 68, 68, 68, 68],
      editableData: Array.from({length: 18}, () => [0, 0]), // Nuevo estado para los valores editables
    }
  }
  render() {
    const state = this.state;
    const data = Array.from({length: 18}, () => [0, 0, 0, 0, 0]);
  
    return (
      <View style={styles.container}>
        <ScrollView horizontal={true}>
          <View>
            <Table borderStyle={{borderColor: '#C1C0B9'}}>
              <Row data={state.tableHead} widthArr={state.widthArr} style={styles.head} textStyle={styles.text}/>
            </Table>
            <ScrollView style={styles.dataWrapper}>
              <Table borderStyle={{borderColor: '#C1C0B9'}}>
                {
                  data.map((dataRow, index) => (
                    <Row
                      key={index}
                      data={dataRow.map((value, colIndex) => {
                        if (colIndex >= dataRow.length - 2) {
                          return (
                            <TextInput
                              key={colIndex}
                              value={state.editableData[index][colIndex - (dataRow.length - 2)].toString()} // Usa el valor de editableData en lugar de data
                              onChangeText={(text) => {
                                const newEditableData = [...state.editableData];
                                newEditableData[index][colIndex - (dataRow.length - 2)] = text.replace(/[^0-9]/g, '');
                                this.setState({ editableData: newEditableData });
                              }}
                              style={styles.input}
                              keyboardType="numeric"
                            />
                          );
                        } else {
                          return <Text key={colIndex}>{value}</Text>;
                        }
                      })}
                      widthArr={state.widthArr}
                      style={[styles.row, index % 2 && { backgroundColor: '#ffffff' }]}
                      textStyle={styles.text}
                    />
                  ))
                }
              </Table>
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
    paddingTop: 30,
    backgroundColor: '#ffffff'
  },
  head: {
    height: 30,
    backgroundColor: '#E32D39'
  },
  text: {
    textAlign: 'center',
    fontWeight: '200'
  },
  dataWrapper: {
    marginTop: -1
  },
  row: {
    height: 40,
    backgroundColor: '#F7F8FA'
  },
    input: {
      width: 80,
      textAlign: 'center',
      fontWeight: '200'
    },
});
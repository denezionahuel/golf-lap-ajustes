import React, { useState } from 'react';
import { View, Text, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Image } from 'react-native';
import { Linking } from 'react-native';
import logo from '../assets/lap-logo.jpg';

let user = 'demo';
let pwd = 'demo';

type Props = {
  onLogin: () => void;
};

const Login: React.FC<Props> = ({ onLogin }) => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('demo');
  const [password, setPassword] = useState('demo');

  const handleLogin = () => {
    if (username === user && password === pwd) {
      navigation.navigate('Main');
    } else {
      alert('Usuario o clave incorrecto. Intente nuevamente.');
    }
  };

  return (
    <View style={styles.registrationContainer}>
      <View style={styles.lapContainer}>
        <Image style={styles.lap} source={logo} />
      </View>
      <Text style={styles.title}>Bienvenido</Text>
      <TextInput
        style={styles.input}
        placeholder="Matrícula"
        onChangeText={(text) => setUsername(text)}
        value={username}
      />
      <TextInput
        style={styles.input}
        placeholder="Clave"
        onChangeText={(text) => setPassword(text)}
        secureTextEntry={true}
        value={password}
      />
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={handleLogin}>
          <Text style={styles.buttonText}>Ingresar</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => Linking.openURL('https://google.com')}>
          <Text style={styles.reiniciarButton}>Reiniciar clave</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  registrationContainer: {
    backgroundColor: "rgba(0, 0, 0, 0)",
    border: "7px solid",
    borderRadius: 8,
    width: 320,
    height: 520,
    maxWidth: "100%",
    marginTop: 30,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    borderRightColor: "#E32D39",
    borderBottomColor: "#E32D39",
    borderTopColor: "#254B9A",
    borderLeftColor: "#254B9A",
    alignSelf: "center",
  },
  lapContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 130,
    height: 130,
    borderRadius: 65,
    marginTop: 30,
    borderRightColor: "#E32D39",
    borderBottomColor: "#E32D39",
    borderTopColor: "#254B9A",
    borderLeftColor: "#254B9A",
    borderWidth: 9,
    overflow: 'hidden',
    backgroundColor: 'rgba(255, 255, 255, 0.8)',
  },
  lap: {
    width: 85,
    height: 85,
    resizeMode: 'contain',
    marginTop: 10,
  },
  input: {
    height: 40,
    marginBottom: 10,
    paddingHorizontal: 10,
    width: 200,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginTop: 15,
    color: '#313131',
    fontSize: 16,
  },
  button: {
    backgroundColor: '#E32D39',
    borderRadius: 4,
    padding: 10,
    marginBottom: 10,
    width: 100,
  },
  title : {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 20,
    color: '#E32D39',
    marginTop: 30,
    alignSelf: 'center',
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
  },
  reiniciarButton: {
    fontSize: 12,
    margin: 10,
    textDecorationLine: 'none',
    color: 'blue',
    textAlign: 'center'
  },
  buttonContainer: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginTop: 31,
    alignSelf: 'center',
  }
});

export default Login;
import React, { useState } from 'react';
import { View, TextInput, TouchableOpacity, Text, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import BouncyCheckbox from "react-native-bouncy-checkbox";

type Props = {
  onSelectPlayers: (players: string[]) => void;
};

const SelectPlayers: React.FC<Props> = ({ onSelectPlayers }) => {
  const maxPlayers = 4;
  const [players, setPlayers] = useState<string[]>(Array(maxPlayers).fill(''));
  const navigation = useNavigation();

  const handlePlayerNameChange = (index: number, name: string) => {
    const updatedPlayers = [...players];
    updatedPlayers[index] = name;
    setPlayers(updatedPlayers);
  };

  const handleNext = () => {
    if (players.filter((player) => player.trim() !== '').length > 1) {
      navigation.navigate('Scorecard');
    } else {
      alert('Ingrese al menos dos jugadores');
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.playerInputs}>
        {players.map((player, index) => (
          <View style={styles.twoColumns}>
            <TextInput
              key={index}
              style={styles.playerInput}
              placeholder={`Jugador ${index + 1}`}
              value={player}
              onChangeText={(text) => handlePlayerNameChange(index, text)}
            />
            {index > 0 && // show if not logged user
            <BouncyCheckbox
            size={25}
            fillColor="red"
            unfillColor="#FFFFFF"
            text="Seguir jugador"
            iconStyle={{ borderColor: "#E32D39" }}
            innerIconStyle={{ borderWidth: 2 }}
            textStyle={{ 
                fontFamily: "Arial",
                textDecorationLine: 'none',
            }}
            onPress={(isChecked: boolean) => {}}
            />
            }
          </View>
        ))}
      </View>
      <View>
      </View>
      <TouchableOpacity style={styles.button} onPress={(handleNext)}>
        <Text style={styles.buttonText}>Iniciar juego</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',    
    alignItems: 'center',
    justifyContent: 'center',
  },  
  twoColumns: {
    flex: 1,
    flexDirection: 'row',    
  }, 
  playerInputs: {
    marginBottom: 10,
  },
  playerInput: {
    height: 40,
    marginBottom: 10,
    paddingHorizontal: 10,
    width: 200,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginTop: 1,
    color: '#313131',
    fontSize: 16,
    padding: 40,
  },
  button: {
    backgroundColor: '#E32D39',
    borderRadius: 4,
    padding: 10,
    marginBottom: 10,
    width: 110,
  },
  buttonText: {
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
  },
});

export default SelectPlayers;